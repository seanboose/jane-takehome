Version: `1.0.0`
[![Build Status](https://app.travis-ci.com/seanboose/jane-takehome.svg?branch=main)](https://app.travis-ci.com/seanboose/jane-takehome)
# LEAGUE RANKER #

This cutting edge tool provides lightning fast single-threaded parsing of your favorite soccer match data (provided it's formatted as necessary). No longer shall you find yourself the wielder of properly formatted match data without insights into which team is truly the best (in fact, this tool enables you to know the top THREE teams on any given match day!).

Fun fact: this tool is gametype-agnostic. It doesn't even care if you call soccer football.

### How does it work? ###

* Reads lines formatted as `${Team Name} ${Score}, ${Team Name} ${Score}`
* Data can be fed in through `stdin` or you can provide a `filepath` argument for static datasets.
* Each match day's league rankings are output to `stdout` once all matches for the day have been consumed.

### How do I get set up? ###

* Clone the repository
* Install dependencies with `npm i`
* Start with `npm start ${filepath}` or pipe data through stdin
* Run tests with `npm t`
* Add it as a dependency for your other npm projects!

### Development Notes

* The tool's algorithm is as such: first, it reads the input (a provided file or `stdin`) line by line. It parses each line with a regex that grabs the team names and scores from the line. If the regex does not match, it throws away the line and moves to the next line. If it DOES match, it determines the winner (or a tie) and puts their info into a map of `{ [teamName]: points }` for the matchday. If one of the teams is already present in the matchday map, then the matchday has ended. When a matchday has ended (or when the end of the input has been reached), it flushes the map data into another object of the same structure that tracks the overall league points and then prints out the top three teams for the day. Tied teams are sorted alphabetically.
* This algorithm runs with `O(n)` time (`n` = number of lines) and `O(m)` space (`m` = number of teams). It outputs while reading so it is capable of handling an incoming stream of data.
* The file reader and point ranking logic are both located in a single `LeagueRanker` class. Their logic is pretty tightly coupled and the reading is just node's `readLine` package, so I didn't bother making separate `reader` and `ranker` classes for the exercise. Since writing is less tightly coupled and it's reasonable to expect that the requirements for output could change in the future, the writing logic is a contained in a collection of utility functions located in the `printerUtils` file.
* If an `inputPath` argument is not provided to the utility when ran, it falls back to using `/dev/stdin` to capture stdin data. Output is writting to `stdout`.
* Feel free to flex this utility by piping in massive amounts of data. The standard javascript number can handle values far in excess of what's necessary to parse hundreds of terabytes of match data.

### Thoughts and next steps ###

* I tried for a while to create a good integration test with Jest, but capturing stdout on a streaming process proved to be more difficult than what seemed reasonable. If this were production code, I'd likely create a python script (or similar) that runs the process with a sample input, captures stdout, and compares that to an expected output. I decided that such a test was outside the scope of this assignment.
* I didn't realize that the default merge option for bitbucket does not squash commits, so my `main` commit history is much messier than I'd like. I considered redoing the history and force pushing it out, but figured that's probably not necessary for this exercise. In a working environment, I'd hope that some git standards have already been set that would help prevent me from creating an embarrassing commit history.
* In a real project I'd require linting for successful compilation. I'd normally do that with a webpack configuration. For this assignment, however, spinning up a webpack build seemed like overkill, so I just had my IDE lint on save and then compiled with tsc.
* Since the prompt says that input could be on the order of terabytes, I looked into possible constraints that might impose. Javascript's Number.MAX_SAFE_INTEGER is 9007199254740991, which is significantly higher than the highest possible score from terabyte scale input.
* If this were a real package for distribution in any way, I'd flesh out the CLI a bit with yargs or a CLI framework. Just capturing a filepath from argv seemed reasonable for this use case though.

### Project Stack

* ![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)
* ![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
* ![Jest](https://img.shields.io/badge/-jest-%23C21325?style=for-the-badge&logo=jest&logoColor=white)
* ![ESLint](https://img.shields.io/badge/ESLint-4B3263?style=for-the-badge&logo=eslint&logoColor=white)
