#!/usr/bin/env node
import { LeagueRanker } from './leagueRanker';

function start() {
  const inputPath = process.argv[2] || '/dev/stdin';
  const leagueRanker = new LeagueRanker(inputPath);
  leagueRanker.start();
}
start();
