import fs from 'fs';
import readline from 'readline';
import { printMatchDay } from './printerUtils';
import { MatchResults, MatchLeaguePoints, TeamLeaguePoints, TotalMatchDayPoints, TotalLeaguePoints } from './types';

export class LeagueRanker {
  // matches eg 'Team Name One 3, Team Name Two 1'
  private MATCH_REGEX = /^(.*) (\d), (.*) (\d)$/;
  private inputPath: string;
  private day = 1;
  private matchDayPoints: TotalMatchDayPoints = {};
  private leaguePoints: TotalLeaguePoints = {};
  winPoints = 3;
  losePoints = 0;
  tiePoints = 1;

  constructor(inputPath: string) {
    this.inputPath = inputPath;
  }

  public start = (): void => {
    const input = fs.createReadStream(this.inputPath);
    const readInterface = readline.createInterface({ input });
    readInterface.on('line', this.consumeLine);
    readInterface.on('close', this.endMatchDay);
  };

  consumeLine = (line: string): void => {
    const matchResults = this.parseMatch(line);
    if (!matchResults) return;
    const matchLeaguePoints = this.getLeaguePointsForMatch(matchResults);
    if (this.isMatchDayOver(matchLeaguePoints)) this.endMatchDay();
    this.addLeaguePointsToMatchDay(matchLeaguePoints);
  };

  parseMatch = (line: string): MatchResults | void => {
    const parts = this.MATCH_REGEX.exec(line);
    if (!parts) return;
    const teamOne = parts[1];
    const teamTwo = parts[3];
    const scoreOne = Number.parseInt(parts[2]);
    const scoreTwo = Number.parseInt(parts[4]);
    return { teamOne, teamTwo, scoreOne, scoreTwo };
  };

  getLeaguePointsForMatch = (match: MatchResults): MatchLeaguePoints => {
    const { teamOne, teamTwo, scoreOne, scoreTwo } = match;
    let pointsOne;
    let pointsTwo;
    if (scoreOne > scoreTwo) {
      pointsOne = this.winPoints;
      pointsTwo = this.losePoints;
    } else if (scoreOne < scoreTwo) {
      pointsOne = this.losePoints;
      pointsTwo = this.winPoints;
    } else {
      pointsOne = this.tiePoints;
      pointsTwo = this.tiePoints;
    }
    return { teamOne, teamTwo, pointsOne, pointsTwo };
  };

  isMatchDayOver = (points: MatchLeaguePoints): boolean => {
    const { teamOne, teamTwo } = points;
    return this.isTeamPlayedToday(teamOne) || this.isTeamPlayedToday(teamTwo);
  };

  isTeamPlayedToday = (team: string): boolean => {
    return this.matchDayPoints[team] != null;
  };

  addLeaguePointsToMatchDay = (points: MatchLeaguePoints): void => {
    const { teamOne, teamTwo, pointsOne, pointsTwo } = points;
    this.matchDayPoints[teamOne] = pointsOne;
    this.matchDayPoints[teamTwo] = pointsTwo;
  };

  endMatchDay = (): void => {
    this.addAllMatchesToLeague();
    this.startNewMatchDay();
    const topThree = this.getTopThreeTeams(this.leaguePoints);
    printMatchDay(topThree, this.day);
    this.incrementDay();
  };

  startNewMatchDay = () => {
    this.matchDayPoints = {};
  };

  incrementDay = () => {
    this.day += 1;
  };

  addAllMatchesToLeague = (): void => {
    Object.entries(this.matchDayPoints).forEach(([teamName, points]) => this.addMatchToLeague(teamName, points));
  };

  addMatchToLeague = (teamName: string, points: number) => {
    const currentPoints = this.leaguePoints[teamName];
    const newPoints = currentPoints != null ? currentPoints + points : points;
    this.leaguePoints[teamName] = newPoints;
  };

  getTopThreeTeams = (leaguePoints: Record<string, number>): TeamLeaguePoints[] => {
    // TODO need to sort alphabetically too
    const ranked = Object.entries(leaguePoints)
      .sort((a, b) => {
        if (a[1] > b[1]) return -1;
        if (a[1] < b[1]) return 1;
        if (a[0] < b[0]) return -1;
        if (a[0] > b[0]) return 1;
        return 0;
      })
      .map(([team, points]) => ({ team, points }));
    return ranked.slice(0, 3);
  };
}
