import { TeamLeaguePoints } from './types';

export const printMatchDay = (topTeams: TeamLeaguePoints[], matchDay: number): void => {
  writeLine(makeDayLine(matchDay));
  topTeams.map(makeTeamLine).forEach(writeLine);
  writeLine('');
};

export const makeDayLine = (matchDay: number): string => {
  return `Matchday ${matchDay}`;
};

export const makeTeamLine = (total: TeamLeaguePoints): string => {
  const suffix = total.points === 1 ? 'pt' : 'pts';
  const teamLine = `${total.team}, ${total.points} ${suffix}`;
  return teamLine;
};

const writeLine = (line: string): void => {
  process.stdout.write(`${line}\n`);
};
