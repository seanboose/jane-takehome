import path from 'path';
import { LeagueRanker } from './leagueRanker';
import { MatchLeaguePoints, MatchResults, TeamLeaguePoints, TotalLeaguePoints } from './types';

describe('ranker', () => {
  let ranker: LeagueRanker;

  const testFilePath = path.resolve(__dirname, '../assets/test.txt');
  const teamOne = 'teamOne';
  const teamTwo = 'teamTwo';
  const teamThree = 'teamThree';
  const teamFour = 'teamFour';
  const winScore = 2;
  const loseScore = 0;
  const tieScore = 1;

  const addFirstMatchToMatchDay = () => {
    const firstMatch: MatchLeaguePoints = {
      teamOne,
      teamTwo,
      pointsOne: ranker.winPoints,
      pointsTwo: ranker.losePoints,
    };
    ranker.addLeaguePointsToMatchDay(firstMatch);
  };

  beforeEach(() => {
    ranker = new LeagueRanker(testFilePath);
  });
  it('parseMatch parses valid lines', () => {
    const match = `${teamOne} ${winScore}, ${teamTwo} ${loseScore}`;
    const expected: MatchResults = {
      teamOne,
      teamTwo,
      scoreOne: winScore,
      scoreTwo: loseScore,
    };
    const actual = ranker.parseMatch(match);
    expect(actual).toEqual(expected);
  });
  it('parseMatch skips invalid lines', () => {
    const match = 'one, two3';
    const actual = ranker.parseMatch(match);
    expect(actual).toBeFalsy();
  });
  it('getLeaguePointsForMatch scores teamOne win correctly', () => {
    const teamOneWin: MatchResults = {
      teamOne,
      teamTwo,
      scoreOne: winScore,
      scoreTwo: loseScore,
    };
    const expected: MatchLeaguePoints = {
      teamOne,
      teamTwo,
      pointsOne: ranker.winPoints,
      pointsTwo: ranker.losePoints,
    };
    const actual = ranker.getLeaguePointsForMatch(teamOneWin);
    expect(actual).toEqual(expected);
  });
  it('getLeaguePointsForMatch scores teamTwo win correctly', () => {
    const teamOneLose: MatchResults = {
      teamOne,
      teamTwo,
      scoreOne: loseScore,
      scoreTwo: winScore,
    };
    const expected: MatchLeaguePoints = {
      teamOne,
      teamTwo,
      pointsOne: ranker.losePoints,
      pointsTwo: ranker.winPoints,
    };
    const actual = ranker.getLeaguePointsForMatch(teamOneLose);
    expect(actual).toEqual(expected);
  });
  it('getLeaguePointsForMatch scores teamOne win correctly', () => {
    const tieMatch: MatchResults = {
      teamOne,
      teamTwo,
      scoreOne: tieScore,
      scoreTwo: tieScore,
    };
    const expected: MatchLeaguePoints = {
      teamOne,
      teamTwo,
      pointsOne: ranker.tiePoints,
      pointsTwo: ranker.tiePoints,
    };
    const actual = ranker.getLeaguePointsForMatch(tieMatch);
    expect(actual).toEqual(expected);
  });
  it('isMatchDayOver is false no team has played', () => {
    const points: MatchLeaguePoints = { teamOne, teamTwo, pointsOne: ranker.winPoints, pointsTwo: ranker.losePoints };
    expect(ranker.isMatchDayOver(points)).toBeFalsy();
  });
  it('isMatchDayOver is true when teamOne has played', () => {
    const secondMatch: MatchLeaguePoints = {
      teamOne,
      teamTwo: teamThree,
      pointsOne: ranker.winPoints,
      pointsTwo: ranker.losePoints,
    };
    addFirstMatchToMatchDay();
    expect(ranker.isMatchDayOver(secondMatch)).toBeTruthy();
  });
  it('isMatchDayOver is true when teamTwo has played', () => {
    const secondMatch: MatchLeaguePoints = {
      teamOne: teamThree,
      teamTwo,
      pointsOne: ranker.winPoints,
      pointsTwo: ranker.losePoints,
    };
    addFirstMatchToMatchDay();
    expect(ranker.isMatchDayOver(secondMatch)).toBeTruthy();
  });
  it('isMatchDayOver is true when both teams have played', () => {
    const secondMatch: MatchLeaguePoints = {
      teamOne,
      teamTwo,
      pointsOne: ranker.winPoints,
      pointsTwo: ranker.losePoints,
    };
    addFirstMatchToMatchDay();
    expect(ranker.isMatchDayOver(secondMatch)).toBeTruthy();
  });
  it('isMatchDayOver is false when neither team has played', () => {
    const secondMatch: MatchLeaguePoints = {
      teamOne: teamThree,
      teamTwo: teamFour,
      pointsOne: ranker.winPoints,
      pointsTwo: ranker.losePoints,
    };
    addFirstMatchToMatchDay();
    expect(ranker.isMatchDayOver(secondMatch)).toBeFalsy();
  });
  it('isTeamPlayedToday is true when team has played', () => {
    addFirstMatchToMatchDay();
    expect(ranker.isTeamPlayedToday(teamOne)).toBeTruthy();
  });
  it('isTeamPlayedToday is false when team has not played', () => {
    addFirstMatchToMatchDay();
    expect(ranker.isTeamPlayedToday(teamThree)).toBeFalsy();
  });

  it('getTopThreeTeams returns top three teams', () => {
    const firstTeam = 'six';
    const firstPoints = 6;
    const secondTeam = 'five';
    const secondPoints = 5;
    const thirdTeam = 'four';
    const thirdPoints = 4;
    const leaguePoints: TotalLeaguePoints = {
      one: 1,
      [firstTeam]: firstPoints,
      two: 2,
      [secondTeam]: secondPoints,
      three: 3,
      [thirdTeam]: thirdPoints,
    };
    const expected: TeamLeaguePoints[] = [
      { team: firstTeam, points: firstPoints },
      { team: secondTeam, points: secondPoints },
      { team: thirdTeam, points: thirdPoints },
    ];
    const actual = ranker.getTopThreeTeams(leaguePoints);
    expect(actual).toEqual(expected);
  });
  it('getTopThreeTeams returns two teams if only two played', () => {
    const firstTeam = 'six';
    const firstPoints = 6;
    const secondTeam = 'five';
    const secondPoints = 5;
    const leaguePoints: TotalLeaguePoints = {
      [firstTeam]: firstPoints,
      [secondTeam]: secondPoints,
    };
    const expected: TeamLeaguePoints[] = [
      { team: firstTeam, points: firstPoints },
      { team: secondTeam, points: secondPoints },
    ];
    const actual = ranker.getTopThreeTeams(leaguePoints);
    expect(actual).toEqual(expected);
  });
  it('getTopThreeTeams sorts ties alphabetically', () => {
    const firstTeam = 'a';
    const firstPoints = 1;
    const secondTeam = 'b';
    const secondPoints = 1;
    const thirdTeam = 'c';
    const thirdPoints = 1;
    const leaguePoints: TotalLeaguePoints = {
      [firstTeam]: firstPoints,
      [secondTeam]: secondPoints,
      [thirdTeam]: thirdPoints,
    };
    const expected: TeamLeaguePoints[] = [
      { team: firstTeam, points: firstPoints },
      { team: secondTeam, points: secondPoints },
      { team: thirdTeam, points: thirdPoints },
    ];
    const actual = ranker.getTopThreeTeams(leaguePoints);
    expect(actual).toEqual(expected);
  });
});
