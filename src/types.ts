export type MatchResults = {
  teamOne: string;
  teamTwo: string;
  scoreOne: number;
  scoreTwo: number;
};

export type MatchLeaguePoints = {
  teamOne: string;
  teamTwo: string;
  pointsOne: number;
  pointsTwo: number;
};

export type TeamLeaguePoints = {
  team: string;
  points: number;
};

export type TotalLeaguePoints = Record<string, number>;
export type TotalMatchDayPoints = Record<string, number>;
