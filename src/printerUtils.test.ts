import { makeDayLine, makeTeamLine } from './printerUtils';
import { TeamLeaguePoints } from './types';

describe('printerUtils', () => {
  it('makeDayLine', () => {
    const day = 1;
    const expected = `Matchday ${day}`;
    const actual = makeDayLine(day);

    expect(actual).toEqual(expected);
  });

  it('makeTeamLine is singular when points === 1', () => {
    const team = 'team';
    const points = 1;
    const teamLeaguePoints: TeamLeaguePoints = { team, points };
    const expected = `${team}, ${points} pt`;
    const actual = makeTeamLine(teamLeaguePoints);

    expect(actual).toEqual(expected);
  });
  it('makeTeamLine is plural when points !== 1', () => {
    const team = 'team';
    const points = 0;
    const teamLeaguePoints: TeamLeaguePoints = { team, points };
    const expected = `${team}, ${points} pts`;
    const actual = makeTeamLine(teamLeaguePoints);

    expect(actual).toEqual(expected);
  });
});
